#include <stdio.h>
#include <stdlib.h>
#include "linkedlist.h"


// Initialize an empty list
void initList(List* list_pointer)
{
    //should create the new list called newL
    List newL;

    //size of empty list set to zero
    //newL.size = 0;

    //initialize head and tail of new empty list to NULL
    newL.head = NULL;
    newL.tail = NULL; //THE TAIL WILL ALWAYS POINT TO NULL - NOT BE NULL ITSELF
}

// Create node containing item, return reference of it.
Node* createNode(void* item)
{
    //allocate new node with malloc
    Node *newNode = malloc(sizeof(Node)); 
    newNode -> data = item; //sets value of the node that was just allocated
    newNode -> next = NULL; //sets pointer to next - might be helpful for the insert functions

    //returns pointer to new node
    return newNode;
}


//----------------HEAD------------------------


// Insert item at start of the list.
void insertAtHead(List* list_pointer, void* item)
{
    //creates node to be added - has data as item and next pointer as null
    Node* n = createNode(item);


    //head only points to NULL if it's an empty list
    if(list_pointer -> head == NULL)        //---------when list is empty------------
    {
        list_pointer -> head = n;
        list_pointer -> head -> next = NULL; //since head is the only item in the list it is also tail
    } else                                  //---------when list isn't empty----------
    {
        Node* former_head =  list_pointer -> head;
        
        //pointer to next node of n is set to current head, therefore making n new head
        n -> next = former_head;
        //now that the next node of n has been set to former head - not written as former_head just to be clear that it the new head is being allocated here
        list_pointer -> head = n;

    }
    
}

// Remove item from start of list and return a reference to it
void* removeHead(List* list_pointer)
{
    //---------when list is empty-------------

    if(list_pointer -> head == NULL)
    {
        return NULL;
    }

    //---------when list isn't empty----------
    
    //represents the head node of the list that is going to get deleted
    Node *former_head = list_pointer -> head;
    void *retdata = former_head -> data; //data held in that node that is gonna get yeeted

    //temporary node that points to what will become the new head - as in, the node following the head that is getting deleted
    Node *temp = former_head -> next;
    free(former_head); //memory deallocated from former_head

    //head is now set to point to the temporary node - as in, what followed the previous node
    list_pointer -> head = temp;


    return retdata;

}


//----------------TAIL------------------------


// Insert new item at the end of list.
void insertAtTail(List* list_pointer, void* item)
{
    //creates node to be added - has data as item and next pointer as null
    Node* n = createNode(item);
    
    if(list_pointer -> head == NULL)        //---------when list is empty-------------
    {
        list_pointer -> head = n;
        list_pointer -> head -> next = NULL; //since head is the only item in the list it is also tail
    } else                                  //---------when list isn't empty----------
    {
        //starting point of walk to tail node is head - named former_tail because that is what we will reach
        Node *former_tail = list_pointer -> head;
        //walks to former_tail 
        while(former_tail -> next != NULL)
        {
            former_tail = former_tail -> next;
        }

        //former tail becomes second to last node
        former_tail -> next = n;
        //by setting the next of the new node to be null we make it the tail
        n -> next = NULL;
    
    }

    
}

// Remove item from the end of list and return a reference to it
void* removeTail(List* list_pointer)
{
    //---------when list is empty-------------

    if(list_pointer -> head == NULL)
    {
        return NULL;
    }
    
    //---------when list isn't empty----------

    //head that youre using to start your walk
    Node *check = list_pointer -> head;
    //node immediately following the one you are currently on
    Node *n = check -> next;
    
    //you need to walk to the current tail, such that Node -> next = tail
    while(n != list_pointer -> tail)
    {
        check = n;
        //this is the node of the current tail that will be deleted
        n = check -> next;
    }

    void *retdata = n -> data;
    //deallocates the memory that had previously been associated with n, which should be the tail
    free(n);

    check -> next = NULL;
    
    return retdata;

}


//----------------INDEX------------------------


// Return item at index
void* itemAtIndex(List* list_pointer, int index)
{
    //start walking to target index from head
    Node *curr = list_pointer -> head;
    int count = 0; //represent

    //combs through the entire linked list stopping at tail
    while(curr!= NULL)
    {
        if(count == index)
        {
            return curr;
        }
        count++;
        curr = curr  -> next;
    }
}

// Insert item at a specified index.
void insertAtIndex(List* list_pointer, int index, void* item)
{
    //creates node to be added
    Node *n  = createNode(item);
    //start walking to target index from head
    Node *curr = list_pointer -> head;

    while(curr != index)
    {
        //only stops when the index we want to insert at is reached
        curr = curr -> next;
    }

    //stores the node after the current so that the inserted one can point to it
    Node* temp = curr -> next;
    curr -> next = n; //the next of the new node is set to what the next of the node at that index was before
    n -> next = temp; //next of new node set to point to what was the next of the old node at this index

}




// Insert item at a specified index and return a reference to it
void* removeAtIndex(List* list_pointer, int index)
{
    //keeps track of node previous to the one we are trying to find that way we can later set its pointer to the one after what we are trying to delete
    Node *curr_prev = list_pointer -> head;
    //start walking to target index from head
    Node *curr = list_pointer -> head -> next;

    while(curr != index)
    {
        //only stops when the index we want to delete is reached
        curr_prev = curr_prev -> next;
        curr = curr -> next;
    }

    Node *temp = curr -> next;
    void *retval = curr -> data;

    curr_prev -> next = temp;

    free(curr);

    return retval;

}



void printList(List* list) {
	Node* node;

  // Handle an empty node. Just print a message.
	if(list->head == NULL) {
		printf("\nEmpty List");
		return;
	}
	
  // Start with the head.
	node = (Node*) list->head;

	printf("\nList: \n\n\t"); 
	while(node != NULL) {
		printf("[ %x ]", node->data);

    // Move to the next node
		node = (Node*) node->next;

		if(node !=NULL) {
			printf("-->");
    }
	}
	printf("\n\n");
}